import React from "react";

import DialogPassword from "./DialogPassword";
import DialogConfirmOpen from "./DialogConfirmOpen";
import DialogConfirmCancel from "./DialogConfirmCancel";

// Router-dom
import { Route, Switch } from "react-router-dom";

// Home Screen
function Home() {
  return (
    <div>
      <DialogPassword />
      <br />
      <DialogConfirmOpen />
      <br />
      <DialogConfirmCancel />
      <br />
    </div>
  );
}

// Password Screen
function PasswordScreen() {
  return (
    <div>
      <h1>Password Screen</h1>
    </div>
  );
}

// Confirm Open Screen
function OpenScreen() {
  return (
    <div>
      <h1>Confirm Open Order Screen</h1>
    </div>
  );
}

// Confirm Close Screen
function CloseScreen() {
  return (
    <div>
      <h1>Confirm Cancel Order Screen</h1>
    </div>
  );
}

// Notfound Screen
function Notfound() {
  return (
    <div>
      <h1 className="txtNotfound" style={{ color: "red" }}>
        404 Notfound
      </h1>
    </div>
  );
}

export default function HomeScreen() {
  return (
    <div>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/passwordscreen" component={PasswordScreen} />
        <Route path="/openscreen" component={OpenScreen} />
        <Route path="/closescreen" component={CloseScreen} />
        <Route component={Notfound} />
      </Switch>
    </div>
  );
}
