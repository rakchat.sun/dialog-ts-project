import "./App.css";
import HomeScreen from "./components/HomeScreen";

import { BrowserRouter } from "react-router-dom";

function App() {
  return (
    <BrowserRouter>
      <HomeScreen />
    </BrowserRouter>
  );
}

export default App;
